import java.util.Scanner;
public class A3_3Fahrkartenautomat {
static Scanner tastatur = new Scanner(System.in);
	
    static double zuZahlenderBetrag; 
    static double eingezahlterGesamtbetrag;
    static double eingeworfeneM�nze;
    static double r�ckgabebetrag;
    static short  ticketsAnzahl;
	public static void main(String[] args) {
		
	       fahrscheinErfassung();
	       
	       geldeinwurf();
	       
	       fahrscheinAusgabe();
	       
	       r�ckgeldberechnung();
	       
	       
	       
	       
	    }
	       
	       // Geldeinwurf
	       // -----------
	      public static void geldeinwurf() {
		       eingezahlterGesamtbetrag = 0.0;
		       zuZahlenderBetrag = ticketsAnzahl * zuZahlenderBetrag;
		       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		       {
		    	   System.out.print("Noch zu zahlen: ");
		    	   System.out.printf("%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
		    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
		    	   eingeworfeneM�nze = tastatur.nextDouble();
		           eingezahlterGesamtbetrag += eingeworfeneM�nze;
		       }
	       }
	      
	      public static void fahrscheinErfassung() {
	    	  System.out.print("Ticketpreis (EURO): ");
		       zuZahlenderBetrag = tastatur.nextDouble();
		       System.out.print("Ticketanzahl: ");
		       ticketsAnzahl = tastatur.nextShort();
	    	 
	    	  
	    	  if (ticketsAnzahl >= 10 || ticketsAnzahl < 0) {
	    	  System.out.println("Die von Ihnen eingegebene Ticketanzahl (" + ticketsAnzahl + ") entspricht nicht der Vorgabe (1-10)");
	    	  System.out.println("Die Anzahl der Tickets wird auf 1 gesetzt");
	    	  ticketsAnzahl = 1;
	    	  }
	      }

	       // Fahrscheinausgabe
	       // -----------------
	      public static void fahrscheinAusgabe() {
		       System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
	       
	       }
	       // R�ckgeldberechnung und -Ausgabe
	       // -------------------------------
	      public static void r�ckgeldberechnung() {
		       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		       if(r�ckgabebetrag > 0.0)
		       {
		    	   System.out.print("Der R�ckgabebetrag in H�he von ");
		    	   System.out.printf("%.2f", r�ckgabebetrag);
		    	   System.out.print(" EURO");
		    	   System.out.println(" wird in folgenden M�nzen ausgezahlt:");
		
		           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
		           {
		        	  System.out.println("2 EURO");
			          r�ckgabebetrag -= 2.0;
		           }
		           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
		           {
		        	  System.out.println("1 EURO");
			          r�ckgabebetrag -= 1.0;
		           }
		           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
		           {
		        	  System.out.println("50 CENT");
			          r�ckgabebetrag -= 0.5;
		           }
		           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
		           {
		        	  System.out.println("20 CENT");
		 	          r�ckgabebetrag -= 0.2;
		           }
		           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
		           {
		        	  System.out.println("10 CENT");
			          r�ckgabebetrag -= 0.1;
		           }
		           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
		           {
		        	  System.out.println("5 CENT");
		 	          r�ckgabebetrag -= 0.05;
		           }
		       }
		
		       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
		                          "vor Fahrtantritt entwerten zu lassen!\n"+
		                          "Wir w�nschen Ihnen eine gute Fahrt.");
	}

}
